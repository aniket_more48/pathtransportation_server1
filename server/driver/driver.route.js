const express = require('express');
const validate = require('express-validation');
const paramValidation = require('../../config/param-validation');
const driverCtrl = require('./driver.controller');
const { driverAuthenticate } = require('../auth/auth.controller');

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  /** DEV RELATED ROUTE GET /api/drivers - Get list of drivers  To Be Removed later*/
  .get(driverCtrl.list)

  /** POST /api/drivers - Create new driver */
  .post(driverCtrl.create);

router.route('/exists')
/** Check if a phone number is already registered*/
  .post(driverCtrl.checkExists);

router.route('/me')
/** GET /api/drivers/:driverId - Get driver */
  .get(driverAuthenticate, driverCtrl.getMe)

/** PUT /api/drivers/:driverId - Update driver */
  .put(driverAuthenticate, driverCtrl.update)

/** DELETE /api/drivers/:driverId - Delete driver */
  .delete(driverAuthenticate, driverCtrl.remove);

/** LOGOUT /api/drivers/me/token - Delete driver token*/
router.route('/me/token')
  .delete(driverAuthenticate, driverCtrl.removeToken);

/** DEV RELATED ROUTES To Be Removed Later*/
router.route('/:driverId')
  /** GET /api/drivers/:driverId - Get driver */
  .get(driverCtrl.get)

  /** PUT /api/drivers/:driverId - Update driver */
  .put(driverCtrl.update)

  /** DELETE /api/drivers/:driverId - Delete driver */
  .delete(driverCtrl.remove);


/** Load driver when API with driverId route parameter is hit */
router.param('driverId', driverCtrl.load);

module.exports = router;
