const Promise = require('bluebird');
const mongoose = require('mongoose');
const httpStatus = require('http-status');
const APIError = require('../helpers/APIError');
const validator = require('validator');
const jwt = require('jsonwebtoken');
const _ = require('lodash');
const bcrypt = require('bcryptjs');
const config = require('../../config/config');

/**
 * Driver Schema
 */
const DriverSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true
  },
  mobileNumber: {
    type: String,
    required: true,
    trim: true,
    unique: true,
    match: [/^[1-9][0-9]{9}$/, 'The value of path {PATH} ({VALUE}) is not a valid mobile number.']
  },
  password: {
    type: String,
    require: true,
    minlength: 6
  },
  email: {
    type: String,
    trim: true,
    lowercase: true,
    unique: true,
    match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, 'Please fill a valid email address']
  },
  dob: {
    type: Date
  },
  drivingLicense: {
    type: String
  },
  licenseValidity: {
    type: String
  },
  driverSsn: {
    type: String
  },
  vehicleId: {
    type: String
  },
  numOfTrips: {
    type: Number
  },
  rating: {
    type: Number
  },
  rides: [{
    rideId: String
  }],
  location: {
    type: String
  },
  lastSeen: {
    type: Date
  },
  isActive: {
    type: Boolean
  },
  onRide: {
    type: Boolean
  },
  tokens: [{
    access: {
      type: String,
      required: true
    },
    token: {
      type: String,
      required: true
    }
  }],
  emailVerified: {
    type: Boolean
  },
  phoneVerified: {
    type: Boolean
  },
  documentsVerified: {
    type: Boolean
  },
  createdAt: {
    type: Date,
    default: Date.now
  }
});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */
DriverSchema.methods = {
  toJSON() {
    const driver = this;
    const userObject = driver.toObject();

    return _.pick(userObject, ['_id', 'username', 'mobileNumber', 'email', 'gender', 'dob']);
  },

  generateAuthToken() {
    console.log('inside generate auth token');
    const driver = this;
    const access = 'auth';
    const token = jwt.sign({ _id: driver._id.toHexString(), access }, process.env.JWT_SECRET).toString();

    driver.tokens = driver.tokens.concat([{ access, token }]);

    return driver.save().then(() => token);
  },

  removeToken(token) {
    const driver = this;

    return driver.update({
      $pull: {
        tokens: { token }
      }
    });
  }

};


/**
 * Statics
 */

DriverSchema.statics = {
  /**
   * Get driver
   * @param {ObjectId} id - The objectId of driver.
   * @returns {Promise<Driver, APIError>}
   */
  get(id) {
    return this.findById(id)
      .exec()
      .then((driver) => {
        if (driver) {
          return driver;
        }
        const err = new APIError('No such driver exists!', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },

  /**
   * List drivers in descending order of 'createdAt' timestamp.
   * @param {number} skip - Number of drivers to be skipped.
   * @param {number} limit - Limit number of drivers to be returned.
   * @returns {Promise<Driver[]>}
   */
  list({ skip = 0, limit = 50 } = {}) {
    return this.find()
      .sort({ createdAt: -1 })
      .skip(+skip)
      .limit(+limit)
      .exec();
  },


  findByToken(token) {
    const Driver = this;
    let decoded;
    console.log('inside find by token');
    try {
      console.log('inside jtw verify');
      decoded = jwt.verify(token, config.jwtSecret);
    } catch (e) {
      return Promise.reject();
    }

    return Driver.findOne({
      _id: decoded._id,
      'tokens.token': token,
      'tokens.access': 'auth'
    });
  },

  findByCredentials(mobileNumber, password) {
    const Driver = this;

    return Driver.findOne({ mobileNumber }).then((driver) => {
      if (!driver) {
        return Promise.reject();
      }

      return new Promise((resolve, reject) => {
      // Use bcrypt.compare to compare password and driver.password
        bcrypt.compare(password, driver.password, (err, res) => {
          if (res) {
            resolve(driver);
          } else {
            reject();
          }
        });
      });
    });
  },

  findByPhoneNumber(mobileNumber) {
    const Driver = this;

    return Driver.findOne({ mobileNumber }).then((driver) => {
      if (!driver) {
        return Promise.resolve(false);
      }
      return Promise.resolve(driver);
    });
  }

};

DriverSchema.pre('save', function (next) {
  const driver = this;

  if (driver.isModified('password')) {
    bcrypt.genSalt(10, (err, salt) => {
      bcrypt.hash(driver.password, salt, (err, hash) => {
        driver.password = hash;
        next();
      });
    });
  } else {
    next();
  }
});


/**
 * @typedef Driver
 */
const Driver = mongoose.model('Driver', DriverSchema);

module.exports = { Driver };
