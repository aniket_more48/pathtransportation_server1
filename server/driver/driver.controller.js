const { Driver } = require('./driver.model');
const _ = require('lodash');

/**
 * Load driver and append to req.
 */
function load(req, res, next, id) {
  Driver.get(id)
    .then((driver) => {
      req.driver = driver; // eslint-disable-line no-param-reassign
      return next();
    })
    .catch(e => next(e));
}

/**
 * Get driver
 * @returns {Driver}
 */
function get(req, res) {
  return res.json(req.driver);
}

/**
 * Get driver
 * @returns {Driver}
 */
function getMe(req, res) {
  return res.json(req.driver);
}

/**
 * Create new user
 * @property {string} req.body.username - The username of driver.
 * @property {string} req.body.mobileNumber - The mobileNumber of driver.
 * @returns {Driver}
 */
function create(req, res, next) {
  const body = _.pick(req.body, ['username', 'mobileNumber', 'password', 'email', 'dob']);
  const driver = new Driver(body);
  driver.save()
    .then(() => driver.generateAuthToken()).then((token) => {
      res.header('x-auth', token).send(driver);
    })
    .catch(e => next(e));
}

/**
 * Update existing driver
 * @property {string} req.body.username - The username of driver.
 * @property {string} req.body.mobileNumber - The mobileNumber of driver.
 * @returns {Driver}
 */
function update(req, res, next) {
  const body = _.pick(req.body, ['username', 'mobileNumber', 'password', 'email', 'dob']);
  const id = req._id;

  Driver.get(id)
    .then((driver) => {
      if (!driver) {
        return res.status(404).send();
      }
      const updateDriverObj = _.merge(driver, body);
      const updatedDriver = new Driver(updateDriverObj);
      updatedDriver.save()
        .then(drivers => res.json(drivers));
    })
    .catch(e => next(e));
}


function removeToken(req, res) {
  req.driver.removeToken(req.token)
    .then(() => {
      res.status(200).send();
    }, () => {
      res.status(400).send();
    });
}

/** ---------- OLD DEV RELATED */
/**
 * Get driver list.
 * @property {number} req.query.skip - Number of drivers to be skipped.
 * @property {number} req.query.limit - Limit number of drivers to be returned.
 * @returns {Driver[]}
 */
function list(req, res, next) {
  const { limit = 50, skip = 0 } = req.query;
  Driver.list({ limit, skip })
    .then(drivers => res.json(drivers))
    .catch(e => next(e));
}

/**
 * Delete driver.
 * @returns {Driver}
 */
function remove(req, res, next) {
  const driver = req.driver;
  driver.remove()
    .then(deletedDriver => res.json(deletedDriver))
    .catch(e => next(e));
}

/**
 * Check if phone number is already registered.
 * @returns {Boolean}
 */
function checkExists(req, res, next) {
  const body = _.pick(req.body, ['mobileNumber']);
  Driver.findByPhoneNumber(body.mobileNumber)
    .then((exists) => {
      if (exists === false) {
        res.json({ status: false });
      } else { res.json({ status: true, username: exists.username }); }
    })
    .catch(e => next(e));
}

module.exports = { load, get, getMe, create, update, removeToken, list, remove, checkExists };
