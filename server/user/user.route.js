const express = require('express');
const validate = require('express-validation');
const paramValidation = require('../../config/param-validation');
const userCtrl = require('./user.controller');
const { userAuthenticate } = require('../auth/auth.controller');

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  /** DEV RELATED ROUTE GET /api/users - Get list of users  To Be Removed later*/
  .get(userCtrl.list)

  /** POST /api/users - Create new user */
  .post(userCtrl.create);

router.route('/exists')
  /** Check if a phone number is already registered*/
  .post(userCtrl.checkExists);

router.route('/me')
/** GET /api/users/:userId - Get user */
  .get(userAuthenticate, userCtrl.getMe)

/** PUT /api/users/:userId - Update user */
  .put(userAuthenticate, userCtrl.update)

/** DELETE /api/users/:userId - Delete user */
  .delete(userAuthenticate, userCtrl.remove);

/** LOGOUT /api/users/me/token - Delete user token*/
router.route('/me/token')
  .delete(userAuthenticate, userCtrl.removeToken);

/** DEV RELATED ROUTES To Be Removed Later*/
router.route('/:userId')
  /** GET /api/users/:userId - Get user */
  .get(userCtrl.get)

  /** PUT /api/users/:userId - Update user */
  .put(validate(paramValidation.updateUser), userCtrl.update)

  /** DELETE /api/users/:userId - Delete user */
  .delete(userCtrl.remove);


/** Load user when API with userId route parameter is hit */
router.param('userId', userCtrl.load);

module.exports = router;
