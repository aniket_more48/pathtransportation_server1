const Promise = require('bluebird');
const mongoose = require('mongoose');
const httpStatus = require('http-status');
const APIError = require('../helpers/APIError');
const validator = require('validator');
const jwt = require('jsonwebtoken');
const _ = require('lodash');
const bcrypt = require('bcryptjs');
const config = require('../../config/config');

/**
 * User Schema
 */
const UserSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true
  },
  mobileNumber: {
    type: String,
    required: true,
    trim: true,
    unique: true,
    match: [/^[1-9][0-9]{9}$/, 'The value of path {PATH} ({VALUE}) is not a valid mobile number.']
  },
  password: {
    type: String,
    require: true,
    minlength: 6
  },
  email: {
    type: String,
    trim: true,
    lowercase: true,
    unique: true,
    match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, 'Please fill a valid email address']
  },
  dob: {
    type: Date
  },
  numOfTrips: {
    type: Number
  },
  rating: {
    type: Number
  },
  rides: [{
    rideId: String
  }],
  location: {
    type: String
  },
  lastSeen: {
    type: Date
  },
  paymentDetails: [{
    paymentId: String
  }],
  tokens: [{
    access: {
      type: String,
      required: true
    },
    token: {
      type: String,
      required: true
    }
  }],
  emailVerified: {
    type: Boolean
  },
  phoneVerified: {
    type: Boolean
  },
  createdAt: {
    type: Date,
    default: Date.now
  }
});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */
UserSchema.methods = {
  toJSON() {
    const user = this;
    const userObject = user.toObject();

    return _.pick(userObject, ['_id', 'username', 'mobileNumber', 'email', 'gender', 'dob']);
  },

  generateAuthToken() {
    console.log('inside generate auth token');
    const user = this;
    const access = 'auth';
    const token = jwt.sign({ _id: user._id.toHexString(), access }, process.env.JWT_SECRET).toString();

    user.tokens = user.tokens.concat([{ access, token }]);

    return user.save().then(() => token);
  },

  removeToken(token) {
    const user = this;

    return user.update({
      $pull: {
        tokens: { token }
      }
    });
  }

};


/**
 * Statics
 */

UserSchema.statics = {
  /**
   * Get user
   * @param {ObjectId} id - The objectId of user.
   * @returns {Promise<User, APIError>}
   */
  get(id) {
    return this.findById(id)
      .exec()
      .then((user) => {
        if (user) {
          return user;
        }
        const err = new APIError('No such user exists!', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },

  /**
   * List users in descending order of 'createdAt' timestamp.
   * @param {number} skip - Number of users to be skipped.
   * @param {number} limit - Limit number of users to be returned.
   * @returns {Promise<User[]>}
   */
  list({ skip = 0, limit = 50 } = {}) {
    return this.find()
      .sort({ createdAt: -1 })
      .skip(+skip)
      .limit(+limit)
      .exec();
  },


  findByToken(token) {
    const User = this;
    let decoded;
    console.log('inside find by token');
    try {
      console.log('inside jtw verify');
      decoded = jwt.verify(token, config.jwtSecret);
    } catch (e) {
      return Promise.reject();
    }

    return User.findOne({
      _id: decoded._id,
      'tokens.token': token,
      'tokens.access': 'auth'
    });
  },

  findByCredentials(mobileNumber, password) {
    const User = this;

    return User.findOne({ mobileNumber }).then((user) => {
      if (!user) {
        return Promise.reject(false);
      }

      return new Promise((resolve, reject) => {
      // Use bcrypt.compare to compare password and user.password
        bcrypt.compare(password, user.password, (err, res) => {
          if (res) {
            resolve(user);
          } else {
            reject();
          }
        });
      });
    });
  },

  findByPhoneNumber(mobileNumber) {
    const User = this;

    return User.findOne({ mobileNumber }).then((user) => {
      if (!user) {
        return Promise.resolve(false);
      }
      return Promise.resolve(user);
    });
  }

};

UserSchema.pre('save', function (next) {
  const user = this;

  if (user.isModified('password')) {
    bcrypt.genSalt(10, (err, salt) => {
      bcrypt.hash(user.password, salt, (err, hash) => {
        user.password = hash;
        next();
      });
    });
  } else {
    next();
  }
});


/**
 * @typedef User
 */
const User = mongoose.model('User', UserSchema);

module.exports = { User };
