const express = require('express');
const validate = require('express-validation');
const expressJwt = require('express-jwt');
const paramValidation = require('../../config/param-validation');
const authCtrl = require('./auth.controller');
const config = require('../../config/config');
const { authenticate } = require('../auth/auth.controller');

const router = express.Router(); // eslint-disable-line new-cap

/** POST /api/auth/user/login - Returns token if correct username and password is provided */
router.route('/user/login')
  .post(validate(paramValidation.login), authCtrl.userLogin);

router.route('/driver/login')
  .post(validate(paramValidation.login), authCtrl.driverLogin);

/** GET /api/auth/random-number - Protected route,
 * needs token returned by the above as header. Authorization: Bearer {token} */
router.route('/driver/authenticate')
  .get(authCtrl.userAuthenticate);

router.route('/driver/authenticate')
  .get(authCtrl.driverAuthenticate);

module.exports = router;
