const jwt = require('jsonwebtoken');
const httpStatus = require('http-status');
const APIError = require('../helpers/APIError');
const config = require('../../config/config');
const _ = require('lodash');
const { User } = require('../user/user.model');
const { Driver } = require('../driver/driver.model');

// sample user, used for authentication
const user1 = {
  username: 'react',
  password: 'express'
};

/**
 * Returns jwt token if valid username and password is provided
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function login_jwt(req, res, next) {
  // Ideally you'll fetch this from the db
  // Idea here was to show how jwt works with simplicity
  if (req.body.username === user1.username && req.body.password === user1.password) {
    const token = jwt.sign({
      username: user1.username
    }, config.jwtSecret);
    return res.json({
      token,
      username: user1.username
    });
  }

  const err = new APIError('Authentication error', httpStatus.UNAUTHORIZED, true);
  return next(err);
}

const userLogin = (req, res, next) => {
  const body = _.pick(req.body, ['mobileNumber', 'password']);

  User.findByCredentials(body.mobileNumber, body.password).then(user => user.generateAuthToken().then((token) => {
    res.header('x-auth', token).send(user);
  })).catch((e) => {
    const err = new APIError('Authentication error', httpStatus.UNAUTHORIZED, true);
    return next(err);
  });
};

const driverLogin = (req, res, next) => {
  const body = _.pick(req.body, ['mobileNumber', 'password']);

  Driver.findByCredentials(body.mobileNumber, body.password).then(driver => driver.generateAuthToken().then((token) => {
    res.header('x-auth', token).send(driver);
  })).catch((e) => {
    const err = new APIError('Authentication error', httpStatus.UNAUTHORIZED, true);
    return next(err);
  });
};

const userAuthenticate = (req, res, next) => {
  const token = req.header('x-auth');
  User.findByToken(token).then((user) => {
    if (!user) {
      return Promise.reject();
    }

    req.user = user;
    req.token = token;
    req._id = user._id;
    return next();
  }).catch((e) => {
    const err = new APIError('Authentication error', httpStatus.UNAUTHORIZED, true);
    return next(err);
  });
};

const driverAuthenticate = (req, res, next) => {
  const token = req.header('x-auth');
  Driver.findByToken(token).then((driver) => {
    if (!driver) {
      return Promise.reject();
    }

    req.driver = driver;
    req.token = token;
    req._id = driver._id;
    return next();
  }).catch((e) => {
    const err = new APIError('Authentication error', httpStatus.UNAUTHORIZED, true);
    return next(err);
  });
};

module.exports = { userLogin, driverLogin, userAuthenticate, driverAuthenticate };
