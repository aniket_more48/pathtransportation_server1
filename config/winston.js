const winston = require('winston');
const appRoot = require('app-root-path');

const logger = new (winston.Logger)({
  transports: [
    new (winston.transports.File)({
      level: 'info',
      filename: `${appRoot}/logs/app.log`,
      handleExceptions: true,
      json: true,
      maxsize: 5242880, // 5MB
      maxFiles: 5,
      colorize: false,
    })
  ]
});

module.exports = logger;
